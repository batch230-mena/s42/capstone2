const User = require("../models/user.js");
const Product = require("../models/product.js");
const Order = require("../models/order.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// S42 User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
//		password: bcrypt.hashSync(reqBody.password, 10),
		password: reqBody.password,
		mobileNo: reqBody.mobileNo
	})

	if(newUser.password != ""){
		newUser.password = bcrypt.hashSync(reqBody.password, 10);
	}

	return newUser.save().then((user, error) => {

			if(error){
				console.log("New user - error " + error);
				return false;
			} else {
				console.log("New user is saved.");
				return true;
			}
		} 
	).catch(error => {
		console.log("Register user - error " + error);
		return false;
	})
}

// S42 User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			console.log("Email is not existing.");
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				console.log("Password is correct. Token is generated.");
				return {access: auth.createAccessToken(result)};
			}
			else{
				console.log("Password is incorrect.");
				return false;
			}
		}
	})
}

// S45 Retrieve User Details
module.exports.getUserDetails = (userId, newData) => {
	console.log("userId: " + userId);
	console.log("newData.id: " + newData.id);
	
	if(newData.id == userId){
		return User.findById(userId).then(result => {
			result.password = "";
			return result;
		})
	}
	else{
		let message = Promise.resolve('User Id mismatch with token');
		return message.then((value) => {return value});
	}
}

// Stretch Goal - Set admin user
module.exports.setAdminUser = (userId, newData) => {
	if(newData.isAdmin == true){
		return User.findByIdAndUpdate(userId,
			{
				isAdmin: newData.user.isAdmin 
			}
		).then((result, error)=>{
			if(error){
				console.log("Set admin user - error " + error);
				return false;
			}
			else{
				console.log("User is set as admin user.");
				return true;
			}
		}).catch(error => {
			console.log("Set admin user - isAdmin - error " + error);
			return false;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to set another user as admin');
		return message.then((value) => {return value});
	}
}

// Old Checkout
// S45 Non-admin User checkout (Create Order)
module.exports.checkout = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(request.body.productId === "" || request.body.quantity === 0){
		console.log("Checkout - product id missing or quantity zero.");
		response.send(false);
		return false;
	}
	
	if(userData.isAdmin){
		console.log("User Id must be non-admin to checkout");
		response.send(false);
	}
	else{
		console.log("userData.id: " + userData.id);

		let productStocks = await Product.findById(request.body.productId).then(result => result.stocks);

		let productPrice = await Product.findById(request.body.productId).then(result => result.price);

		let isProductActive = await Product.findById(request.body.productId).then(result => result.isActive);

		let orderID = await Order.findOne({userId:userData.id}).then((order) => {
			if(order._id == null){
				return "";
			}	
			else{
				return order._id;
			}
		}).catch(error => {
			console.log("Checkout - orderID - error " + error);
			return false;
		});

		console.log("orderID: " + orderID);
		
		if(productStocks > 0 && productStocks > request.body.quantity && isProductActive){
			let isProductUpdated = await Product.findById(request.body.productId).then(product => {

				product.stocks -= request.body.quantity;

				return product.save()
				.then(result => {
					console.log("Product stocks is updated.");
					console.log(result);
					return true;
				})
				.catch(error => {
					console.log("Update product stocks - error " + error);
					return false;
				})
			})
		}

		if(productStocks > 0 && productStocks > request.body.quantity && isProductActive){

			if(orderID == ""){

				let newOrder =  new Order({
					totalAmount: (request.body.quantity * productPrice),
					userId: userData.id,
					products: [{
						productId: request.body.productId,
						quantity: request.body.quantity
					}]
				})

				console.log("newOrder: " + newOrder);

				return newOrder.save().then((order, error) => {

					if(error){
						console.log("New order - error " + error);
						response.send(false);
					} 
					else {
						console.log("New order is saved.");
						response.send(true);
					}
				}).catch(error => {
					console.log("Checkout - error " + error);
					response.send(false);
				})
			}
			else{

				let isOrderUpdated = await Order.findById(orderID).then(order => {

					console.log("orderID: " + orderID);
					console.log("order.totalAmount: " + order.totalAmount);
					order.totalAmount += (request.body.quantity * productPrice);

					order.products.push({
						productId: request.body.productId,
						quantity: request.body.quantity
					});
				
					return order.save()
					.then(result => {
						console.log(result);
						console.log("Update order products is saved.");
						response.send(true);
					})
					.catch(error => {
						console.log("Checkout - products update- error " + error);
						response.send(false);
					})
				})
			}
		}
		else{
			if(isProductActive){
				console.log("Not enough stocks.");
			}
			else{
				console.log("Product is not active.");
			}
			response.send(false);
		}
	}
}

// Stretch Goal - Checkout Pay
module.exports.checkoutPay = (userId, newData) => {
	console.log("Checkout Pay - userId: " + userId);
	console.log("Checkout Pay - newData.order.orderId: " + newData.order.orderId);

	if(newData.isAdmin == false){
		return Order.findByIdAndUpdate(newData.order.orderId,
			{
				status: newData.order.status 
			}
		).then((result, error)=>{
			if(error){
				console.log("Checkout pay - error " + error);
				return false;
			}
			else{
				console.log("Order is paid.");
				return true;
			}
		}).catch(error => {
			console.log("Checkout pay find order - error " + error);
			return false;
		})
	}
	else{
		let message = Promise.resolve('User Id must be non-admin to pay for checkout');
		return message.then((value) => {return value});
	}
}

