const Product = require("../models/product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// S43 Create Product (Admin only)
module.exports.addProduct = (newData) => {

	if(newData.isAdmin == true){ 
		let newProduct = new Product({
			name: newData.product.name,
			description: newData.product.description,
			price: newData.product.price,
			stocks: newData.product.stocks
		})

		return newProduct.save().then((newProduct, error) => {

			if(error){
				console.log("Save product - error " + error);
				return error;
			}
			else{
				console.log("New product is saved.");
				return newProduct;
			}
		}).catch(error => {
			console.log("Add product - error " + error);
			return false;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to add product');
		return message.then((value) => {return value});
	}
}

// S43 Retrieve all Active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {

		if(result == null){
			console.log("Get all active products - Not Found");
		}
		else{
			console.log("Get all active products - Found");	
		}

		return result;
	})
}

// S44 Retrieve single product
module.exports.getProduct = (productID) => {
	return Product.findById(productID).then(result => {

		if(result == null){
			console.log("Get single product - Not Found");
		}
		else{
			console.log("Get single product - Found");	
		}

		return result;
	})
}

// S44 Update Product information (Admin only)
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				name: newData.product.name, 
				description: newData.product.description,
				price: newData.product.price,
				stocks: newData.product.stocks
			}
		).then((result, error)=>{
			if(error){
				console.log("Update product - error " + error);
				return false;
			}
			else{
				console.log("Product is updated.");
				return true;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to update product information');
		return message.then((value) => {return value});
	}
}


// S44 Archive Product (Admin only)
module.exports.archiveProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				isActive: newData.product.isActive 
			}
		).then((result, error)=>{
			if(error){
				console.log("Archive product - error " + error);
				return false;
			}
			else{
				console.log("Product is archived.");
				return true;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to archive product');
		return message.then((value) => {return value});
	}
}
