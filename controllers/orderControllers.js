const User = require("../models/user.js");
//const Product = require("../models/product.js");
const Order = require("../models/order.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// Stretch Goal - Retrieve authenticated user’s orders
module.exports.getUserOrders = (userId, newData) => {
	console.log("userId: " + userId);
	console.log("newData.id: " + newData.id);
	
	if(newData.id == userId){
		return Order.find({userId:newData.id}).then(result => {
			if(result.length == 0){
				console.log("Get user orders - Not Found");
			}
			else{
				console.log("Get user orders - Found");	
			}

			return result;
		}).catch(error => {
			console.log("Get user orders - error " + error);
			return false;
		})
	}
	else{
		let message = Promise.resolve('User Id mismatch with token');
		return message.then((value) => {return value});
	}
}

// Stretch Goal - Retrieve all orders (Admin only)
module.exports.getAllOrders = (allData) => {
	if(allData.isAdmin == true){ 

		return Order.find().then(result => {
			return result;
		}).catch(error => {
			console.log("Get all orders - error " + error);
			return false;
		})
	}
	else{
		let message2 = Promise.resolve('User must be ADMIN to retrieve all orders');
		return message2.then((value) => {return value});
	}
}


