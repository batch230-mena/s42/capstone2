const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");


// S42 User Registration
router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})

// S42 User Authentication
router.post("/login", (request, response) => {
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

// S45 Retrieve User Details
router.get("/:userId/userDetails", auth.verify, (request,response) => 
{
	const newData = {
		id: auth.decode(request.headers.authorization).id
	}

	userControllers.getUserDetails(request.params.userId, newData)
	.then(resultFromController => {response.send(resultFromController)})
})

// Stretch Goal - Set admin user
router.patch("/:userId/admin", auth.verify, (request,response) => 
{
	const newData = {
		user: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userControllers.setAdminUser(request.params.userId, newData)
	.then(resultFromController => {
		response.send(resultFromController)
	})
})

// Old checkout
// S45 Non-admin User checkout (Create Order)
router.post("/checkout", auth.verify, userControllers.checkout);

// Stretch Goal - Checkout Pay
router.patch("/:userId/pay", auth.verify, (request,response) => 
{
	const newData = {
		order: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userControllers.checkoutPay(request.params.userId, newData)
	.then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router;
