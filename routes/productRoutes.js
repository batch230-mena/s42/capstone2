const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// S43 Create Product (Admin only)
router.post("/create", auth.verify, (request, response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.addProduct(newData)
	.then(resultFromController => response.send(resultFromController))
})

// S43 Retrieve all Active products
router.get("/active", (request, response) => {
	productControllers.getActiveProducts()
	.then(resultFromController => response.send(resultFromController))
});

// S44 Retrieve single product
router.get("/:productId", (request, response) => {
	productControllers.getProduct(request.params.productId)
	.then(resultFromController => response.send(resultFromController))
});

// S44 Update Product information (Admin only)
router.put("/:productId/update", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.updateProduct(request.params.productId, newData)
	.then(resultFromController => {
		response.send(resultFromController)
	})
})

// S44 Archive Product (Admin only)
router.put("/:productId/archive", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(request.params.productId, newData)
	.then(resultFromController => {
		response.send(resultFromController)
	})
}) 


module.exports = router;
